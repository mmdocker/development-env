#########################
Linux specific docker local development environment instructions.
#########################


Move the _db, _logs, _vhosts, and docker-compose.yml to your root code directory. (This controls where your volumes are mounted, I use ~/code, if you don't use ~/code, you'll need to update aliases and vhosts later).



You'll need a working installation of dnsmasq (I point to .d for my TLD, but you can use whatever you want. .dev no longer works in most browsers). If you don't want to use dnsmasq, you'll have to manually manage your hosts file (painful and time consuming).



Open /etc/network/interfaces and add the following alias. (requires reboot, needed for xdebug)

auto lo:1
iface lo:1 inet static
address 192.168.2.1
network 192.168.2.0
netmask 255.255.255.0



If you want to use portainer (gui based docker controls, kinda useful for cleaning up images/volumes, but not for managing the full docker instance)
Move the portainer folder into a location of your choosing, I would use ~/docker/portainer. (This is just an arbitrary location and has no bearing on other volume mount points.)



Setup each of your sites in the _vhosts folder by copying the appropriate template (or creating your own) and updating the <folder> and <site name> parameters. The folder path needs to match what it will be INSIDE the docker container. Whatever folder you put the docker-compose.yml file into is going to be mapped to /var/www (i.e. <root code directory>/work/myproject will map to /var/www/work/myproject). You can also choose which version of PHP is going to be used by changing the fastcgi_pass parameter of the site (php56 or php71).


Add these aliases to your ~/.aliases or ~/.bash_aliases file (whichever you have/use) to make working with these containers easier. Make sure to update <root code directory> to your actual path (i.e. ~/code)


Source your aliases or close and re-open your terminal for them to take effect
:> . ~/.aliases


IMPORTANT
Although you can technically have multiple web / database servers installed, you can’t run them at the same time. I recommend exporting any databases that need to be imported into the docker install and physically uninstalling any instances of MAMP/XAMMP/Apache/Nginx/MySQL/MariaDB/Redis/etc. to prevent issues with linux trying to auto-start services on boot. At a bare minimum, you will need to stop all of the other web related services before trying to start the docker containers (with the exception of portainer, as it shouldn’t conflict with any other setups).


Start the portainer container to allow you to easily manage individual containers. Accessible @ portainer.d if the nginx container is running, or portainer.d:8000 if not.
:> cd <path to docker folder>/portainer
:> dcu


You can use coreweb to start|stop|restart|rebuild the entire batch of core containers. Starting, stopping, and restarting containers does not pull in new changes from a build process. If an image Dockerfile has been changed and rebuilt, you’ll need to rebuild your coreweb to bring in those changes. Once you run a command with coreweb, run dps to make sure the containers are running or not running as you expect.


Mariadb can be accessed via 127.0.0.1 with user root and no password (using localhost/sockets will NOT work). Your .env files and other project DB configs will need to point to mariadb (localhost/sockets again, will NOT work).


Mailcatcher can be accessed via mailcatcher.d in a browser. PHP sendmail is pointed to mailcatcher in both versions.


After a reboot, xdebug can be accessed via localhost:9009.


Any composer / npm / bower / artisan / etc. commands that need to be run against your project HAVE to be run from inside the appropriate PHP docker container. If you run it from your local machine, you will get unexpected results when it pulls in packages based on the wrong PHP version, or if it needs to access the DB. You can connect to the containers by using drbash <container name> (for non-php containers) or dwbash <container name> (for php containers) from the terminal, or by using the portainer web interface.





##### Common Problems #####

Q: DB host cannot be found
A: Use the docker container name for your db (mariadb) instead of an IP address or localhost/socket

Q: DB times out “MySQL has gone away”
A: Turn OFF persistent connections in your project DB config

Q: DB cannot be found when trying to connect with a client
A: Make sure the docker containers are running (they don’t autostart when your computer starts)

Q: <site>.d not responding
A: Make sure the docker containers are running (they don’t autostart when your computer starts)

Q: <site>.d goes to the wrong site
A: Make sure you have an appropriate _vhosts file for it (must end with .conf) and the nginx container has been restarted to bring in the changes.
